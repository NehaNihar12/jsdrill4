const pairs = function (obj) {
    if (!obj) {
        return [];
    }
    let listOfKeyValuePairs = [];
    for (let key in obj) {
        let pair = [key, obj[key]];
        listOfKeyValuePairs.push(pair);
    }
    return listOfKeyValuePairs;
}
module.exports = pairs;