const values = (obj) => {
    if (!obj) {
        return [];
    }
    let valuesArr = [];
    for (let key in obj) {
        valuesArr.push(obj[key]);
    }
    return valuesArr;
}
module.exports = values;