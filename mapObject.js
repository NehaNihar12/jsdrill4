const mapObject = function (obj, cb) {
    if (!obj || !cb) {
        return null;
    }
    let newObj = {};
    for (let key in obj) {
        
        newObj[key] = cb(obj[key],key)
    }
    return newObj;
}
module.exports = mapObject;