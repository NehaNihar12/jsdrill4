const defaults = function (obj, defaultObj) {
    if (!obj) {
        return null;
    }
    if (defaultObj) {
        for (let key in defaultObj) {
            if (!obj.hasOwnProperty(key)) {
                obj[key] = defaultObj[key];
            }
        }
    }
    return obj;
}
module.exports = defaults;