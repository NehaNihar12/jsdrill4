const invert = function (obj) {
    if (!obj) {
        return null;
    }
    let invertedObj = {};
    for (let key in obj) {
        let newKey = obj[key];
        if(typeof newKey === 'object'){
            newKey=JSON.stringify(newKey);
        }
        invertedObj[newKey] = key;
    }
    return invertedObj;
}

module.exports = invert;
