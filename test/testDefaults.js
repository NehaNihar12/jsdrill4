const defaults = require('../defaults.js')
let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const defaultObj = { name: 'Bruce Wayne', sport: 'Football', location: 'India' };

console.log(defaults(testObject, defaultObj));

//empty object
console.log(defaults({}, defaultObj));

//null default object
console.log(defaults({ name: 'Bruce Wayne', age: 36 }))