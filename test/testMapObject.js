const mapObject = require('../mapObject.js')
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const cb = (val,key) => val+5;

console.log(mapObject(testObject, cb));
//empty object
console.log(mapObject({}, cb));
//null function
console.log(mapObject(testObject))