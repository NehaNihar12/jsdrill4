const invert = require('../invert.js')
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham', sports:{1:'tennis',2:'football'} };

console.log(invert(testObject));
//empty object
console.log(invert({}));
//null object
console.log(invert())